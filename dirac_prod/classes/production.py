from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from copy import deepcopy
import pickle
import json
import re

import six
from six.moves import zip_longest

from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import ProductionRequestClient
from DIRAC.Core.Security.ProxyInfo import getProxyInfo

from ..exceptions import AlreadyRegisteredError, NotRegisteredError
from ..utils import dw, TypedProperty, ensure_list, ensure_set, ensure_value
from .step import STEP_NAME_MAPPING, Step
from .input_dataset import InputDataset

# dump_fcn = pickle.dumps
# load_fcn = pickle.loads
dump_fcn = json.dumps
load_fcn = json.loads


PRODUCTION_DICT_KEYS = [
    'HasSubrequest',
    'NumberOfEvents',
    'RequestPDG',
    'Comments',
    'RetentionRate',
    'RequestAuthor',
    'Description',
    'RequestType',
    'EventType',
    'SimCondDetail',
    'RequestWG',
    'SimCondID',
    'IsModel',
    'FastSimulationType',
    'RequestPriority',
    'RealNumberOfEvents',
    'RequestState',
    'Inform',
    'ProPath',
    'RequestName',
    'SimCondition',
    'ProID',
    'Extra',
    'ProDetail',
    'MasterID',
    'RequestID',
    'ParentID',
]

AUTOMATIC_KEYS = [
    'bk',
    'bkSrTotal',
    'bkTotal',
    'crTime',
    'FinalizationDate',
    'StartingDate',
    'upTime',
    'rqTotal',
]


def _do_submit(prod):
    if prod._id is not None:
        raise RuntimeError("%s has already been submitted" % prod)
    data = prod._to_dict()
    prod._id = dw(ProductionRequestClient().createProductionRequest(data))
    # Workaround for https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/-/merge_requests/900/
    if isinstance(prod, Production) and prod.fast_simulation_type:
        dw(ProductionRequestClient().updateProductionRequest(
            prod._id, {"FastSimulationType": prod.fast_simulation_type}
        ))


def _lookup_simulation_condition(query):
    conditions = dw(BookkeepingClient().getSimulationConditions(query))
    if len(conditions) == 0:
        raise NotImplementedError(repr(query) + " is not known")
    elif len(conditions) != 1:
        raise NotImplementedError(conditions)
    conditions = conditions[0]
    result = {}
    result['SimCondDetail'] = {
        "BeamEnergy": conditions["BeamEnergy"],
        "Generator": conditions["Generator"],
        "Luminosity": conditions["Luminosity"],
        "MagneticField": conditions["MagneticField"],
        "G4settings": conditions["G4settings"],
        "BeamCond": conditions["BeamCond"],
        "DetectorCond": conditions["DetectorCond"],
    }
    result['SimCondID'] = conditions["SimId"]
    result['SimCondition'] = conditions["SimDescription"]
    return result


class SubProduction(object):
    def __init__(self, parent):
        self._id = None
        self._parent = parent
        self._event_type = None
        self._n_events = None
        self._comment = None
        self._auto = {}
        self._junk = {}

    # Required properties
    @property
    def parent(self):
        return self._parent

    @TypedProperty(ensure_value, str)
    def event_type(self):
        pass

    @TypedProperty(ensure_value, int)
    def n_events(self):
        pass

    # Optional properties
    @TypedProperty(ensure_value, str, allow_none=True)
    def comment(self):
        pass

    # Other properties
    @TypedProperty(ensure_value, int, allow_none=True)
    def id(self):
        if self._id is None:
            raise NotRegisteredError()
        return self._id

    @property
    def registered(self):
        return self._id is not None

    # Public methods
    def register(self):
        raise NotImplementedError()

    # Private methods
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self._to_dict() == other._to_dict()

    # Required for Python 2.7 compatibility
    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        try:
            return 'SubProduction(id=%r)' % self.id
        except NotRegisteredError:
            return 'SubProduction(<unregistered>)' % self.id

    def _from_dict(self, data, parent):
        assert set(data.keys()) == set(PRODUCTION_DICT_KEYS + AUTOMATIC_KEYS), data
        assert parent.id == data['ParentID']
        assert all(v is None for k, v in data.items() if k not in [
            'Comments', 'FastSimulationType', 'RetentionRate', 'RequestWG',
            'HasSubrequest', 'NumberOfEvents', 'EventType', 'IsModel',
            'RealNumberOfEvents', 'MasterID', 'RequestID', 'ParentID',
            'bkTotal', 'StartingDate', 'bkSrTotal', 'bk', 'rqTotal', 'FinalizationDate',
            'upTime', 'crTime',
        ]), data

        # Junk properties, included for consistency
        assert data['RealNumberOfEvents'] == data['NumberOfEvents'], data
        assert data['MasterID'] == data['ParentID'], data
        assert data['HasSubrequest'] == 0, data
        assert data['IsModel'] == 0, data
        assert data['FastSimulationType'] in ['None', None], data
        assert data['RequestWG'] in [None, ''], data
        assert data['RetentionRate'] in [1.0, None], data

        # Set the ID as None to avoid triggering AlreadyRegisteredError
        self._id = None
        self._parent = parent
        self.event_type = data['EventType']
        self.n_events = data['NumberOfEvents']
        self.comment = data['Comments']
        self.id = data['RequestID']

        self._auto = {k: data[k] for k in AUTOMATIC_KEYS}
        self._junk = {k: data[k] for k in ['FastSimulationType', 'RequestWG', 'RetentionRate']}

    def _to_dict(self):
        data = {k: None for k in PRODUCTION_DICT_KEYS}

        if self.id:
            data['RequestID'] = self.id
        data['ParentID'] = self.parent.id
        data['EventType'] = self.event_type
        data['NumberOfEvents'] = self.n_events
        data['Comments'] = self.comment
        # data.update(self._auto)

        # Junk properties, included for consistency
        data['RealNumberOfEvents'] = data['NumberOfEvents']
        data['MasterID'] = data['ParentID']
        data['HasSubrequest'] = 0
        data['IsModel'] = 0
        data['RequestWG'] = self._junk.get('RequestWG', None)
        data['RetentionRate'] = self._junk.get('RetentionRate', None)

        data.update(self._junk)

        return data


class Production(object):
    @classmethod
    def from_yaml(cls, **kwargs):
        self = cls()
        self.name = kwargs["name"]
        self.type = kwargs["type"]
        self.author = dw(getProxyInfo())["username"]
        self.priority = kwargs["priority"]
        self.mc_config_version = kwargs["mc_version"]
        self.fast_simulation_type = kwargs.get("fast_simulation_type")
        self.sim_description = kwargs["sim_condition"]
        self.event_types = kwargs["event_types"]
        self.n_events = kwargs["num_events"]
        # RetentionRate
        self.wg = kwargs["wg"]
        self.inform_emails = kwargs["inform"]
        self.steps = [
            Step.from_yaml(conddb_tag=ct, dddb_tag=dt, **step)
            for step, ct, dt in zip_longest(
                kwargs["steps"],
                [kwargs["dbtags"]["CondDB"]],
                [kwargs["dbtags"]["DDDB"]],
                fillvalue="fromPreviousStep",
            )
        ]
        self.processing_pass = "/".join(
            [s.processing_pass for s in self.steps if s.visible]
        )
        return self

    def __init__(self, id=None):
        if id is None:
            self._name = None
            self._author = None
            self._request_type = None
            self._priority = None  # ['1a', '1b', '2a', '2b', None]
            self._input_dataset = None
            self._steps = []
            # Optionals
            self._id = None
            self._state = 'New'  # ['Active', 'Completed', 'Rejected', 'Tech OK', 'Done', 'Submitted', 'Cancelled', 'New', 'Accepted', 'PPG OK', None]
            self._wg = None  # ['', 'IFT', 'BandQ', 'Calo', 'QCD', 'Calib', 'QEE', 'PID', 'RD', 'Charm', 'Tagging', 'BnoC', 'Tracking', 'Luminosity', 'Simulation', 'RTA', 'B2CC', None, 'BNoC', 'HLT', 'B2OC', 'SL']
            self._inform_emails = set()
            self._comment = None
            self._is_model = False
            self._fast_simulation_type = None
            self._retention = 1.0
            self._mc_config_version = None
            self._sub_productions = list()
            self._processing_pass = None
            self._sim_description = None
            self._event_types = []
            self._n_events = []
        else:
            raise NotImplementedError('Add loading of existing requests')

    @property
    def id(self):
        return self._id

    # Required properties
    @TypedProperty(ensure_value, str)
    def name(self):
        pass

    @TypedProperty(ensure_value, str)
    def type(self):
        pass

    @TypedProperty(ensure_value, str)
    def author(self):
        pass

    @TypedProperty(ensure_value, str)
    def priority(self):
        pass

    @TypedProperty(ensure_value, InputDataset, allow_none=True)
    def input_dataset(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def sim_description(self):
        pass

    @TypedProperty(ensure_list, str)
    def event_types(self):
        pass

    @TypedProperty(ensure_list, int)
    def n_events(self):
        pass

    @TypedProperty(ensure_list, Step)
    def steps(self):
        pass

    # Optional properties
    @TypedProperty(ensure_value, str)
    def state(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def wg(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def processing_pass(self):
        pass

    @TypedProperty(ensure_set, str)
    def inform_emails(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def comment(self):
        pass

    @TypedProperty(ensure_value, bool)
    def is_model(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def fast_simulation_type(self):
        pass

    @TypedProperty(ensure_value, float)
    def retention(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def mc_config_version(self):
        pass

    @TypedProperty(ensure_list, SubProduction)
    def sub_productions(self):
        pass

    # Other properties
    @property
    def registered(self):
        return self._id is not None

    # Public methods
    def register(self):
        if self.registered:
            raise AlreadyRegisteredError(self)
        raise NotImplementedError()

    # Private methods
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self._to_dict() == other._to_dict()

    # Required for Python 2.7 compatibility
    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        try:
            return 'Production(id=%r)' % self.id
        except NotRegisteredError:
            return 'Production(<unregistered>)' % self.id

    def check_steps(self, register_steps=False):
        for i, step in enumerate(self.steps):
            if step.registered:
                print("Step", i, "already registered", step)
            elif register_steps:
                step.register()
            else:
                raise RuntimeError("Step %s is not known to LHCbDIRAC" % i)

    def submit(self, register_steps=False):
        # Create the steps
        self.check_steps(register_steps=register_steps)
        # Create the parent in state New
        _do_submit(self)
        # Create any subproductions
        for sub_prod in self.sub_productions:
            assert self.state == "New"
            _do_submit(sub_prod)
        # Move the parent into the submitted state
        self._state = "Submitted"
        dw(ProductionRequestClient().updateProductionRequest(
            self.id,
            {"RequestState": "Submitted"}
        ))

    def _from_dict(self, data):
        assert set(data.keys()) == set(PRODUCTION_DICT_KEYS + AUTOMATIC_KEYS), data
        assert data['IsModel'] in [0, 1], data
        if data['IsModel']:
            assert data['RequestState'] == 'New', data
        if data['Extra'] is not None:
            assert set(load_fcn(data['Extra']).keys()) == {'mcConfigVersion'}, data

        # Junk properties, included for consistency
        assert data['Description'] is None, data
        assert data['MasterID'] is None, data
        assert data['ParentID'] is None, data
        assert data['ProID'] in [1003909, 1008929, 1002911, 1000405, None]
        assert data['RequestPDG'] in ['', None]
        # TODO: assert data['RealNumberOfEvents'] in ???

        # Set the ID as None to avoid triggering AlreadyRegisteredError
        self._id = None

        self._name = data['RequestName']
        self._type = data['RequestType']
        self._author = data['RequestAuthor']
        self._priority = data['RequestPriority']
        self._state = data['RequestState']
        self._wg = data['RequestWG']
        if self._inform_emails is None:
            self._inform_emails = re.split(r'\s*(?:;|,)\s*', data['Inform'].replace(' ', ',').strip())
        else:
            self._inform_emails = set()
        self._comment = data['Comments']
        self._is_model = bool(data['IsModel'])

        raise NotImplementedError()
        # self._sim_condition = data["SimCondition"]
        # try:
        #     self._sim_cond_detail = load_fcn(data["SimCondDetail"])
        # except Exception:
        #     self._sim_cond_detail = None
        # self._pro_path = data["ProPath"]
        # try:
        #     self._pro_detail = load_fcn(data["ProDetail"])
        # except Exception:
        #     self._pro_detail = None

        # MC options
        self._fast_simulation_type = data['FastSimulationType']
        self._retention = data['RetentionRate']
        if data['Extra'] is None:
            self._mc_config_version = None
        else:
            self._mc_config_version = load_fcn(data['Extra'])

        if data['HasSubrequest'] == 0:
            self.event_types = [data["EventType"]]
            self.n_events = [data['NumberOfEvents']]
        else:
            response = dw(ProductionRequestClient().getProductionRequestList(data['RequestID'], '', 'ASC', 0, -1, {}))
            sub_productions = []
            for row in sorted(response['Rows'], lambda x: x["RequestID"]):
                sub_prod = SubProduction(self)
                sub_prod._from_dict(row, self)
                sub_productions.add(sub_prod)
            self.sub_productions = sub_productions
            assert len(self.sub_productions) == data['HasSubrequest'], response
            assert len(self.sub_productions) == response['Total'], response
            self.event_types = [s.event_type for s in self.sub_productions]
            self.n_events = [s.n_events for s in self.sub_productions]

        conditions = _lookup_simulation_condition({
            "SimId": result['SimCondID']
        })
        for key in conditions:
            if conditions[k] != data[k]:
                raise NotImplementedError("Mismatch!", conditions[k], data[k])
        self.sim_description = conditions["SimDescription"]

        # TODO: This shouldn't be needed
        if data['ProDetail'] is not None and load_fcn(data['ProDetail']):
            if data['ProDetail'] is not None and load_fcn(data['ProDetail']):
                step_data = load_fcn(data['ProDetail'])
                all_step_keys = list(STEP_NAME_MAPPING.values()) + ['TRP', 'IFT', 'OFT', 'Html']
                pattern = re.compile(r'p(\d+)(%s)' % '|'.join(all_step_keys))
                for bad_key in ['progress', 'pID']:
                    if bad_key in step_data:
                        # print(data['RequestID'], 'Removing', bad_key, 'key from ProDetail', data['ProDetail'])
                        step_data.pop(bad_key)

                pAll = step_data.pop('pAll')
                pDsc = step_data.pop('pDsc')

                steps = defaultdict(dict)
                for k, v in step_data.items():
                    match = pattern.match(k)
                    i, k = match.groups()
                    steps[int(i)][k] = v
                assert set(steps.keys()) == set(range(1, len(steps) + 1)), steps
                NAME_STEP_MAPPING = {v: k for k, v in STEP_NAME_MAPPING.items()}
                self._steps = []
                for i in range(1, len(steps) + 1):
                    step = steps[i]
                    html = step.pop('Html', None)
                    ifts = step.pop('IFT', '').split(',')
                    ofts = step.pop('OFT', '').split(',')
                    step = {NAME_STEP_MAPPING[k]: v for k, v in step.items()}
                    self._steps += [Step()]
                    self._steps[-1]._from_dict(step)

        else:
            raise NotImplementedError(data['RequestID'], 'Bad data in ProDetail', data['ProDetail'])

        self._id = data['RequestID']

        self._auto = {data[k] for k in AUTOMATIC_KEYS}
        self._junk = {data[k] for k in ['ProID', 'RequestPDG', 'RealNumberOfEvents']}

    def _to_dict(self):
        request = {
            'RequestName': self.name,
            'RequestType': self.type,
            'RequestAuthor': self.author,
            'RequestPriority': self.priority,
            'RequestState': self.state,
            'RequestWG': self.wg,
            'Comments': self.comment,
            'IsModel': int(self.is_model),

            # MC options
            'FastSimulationType': self.fast_simulation_type,
            'RetentionRate': self.retention,

            # # Dynamically generated values
            # 'bk': 2433,
            # 'bkSrTotal': 0,
            # 'bkTotal': 2433,
            # 'crTime': datetime.datetime(2019, 7, 11, 18, 27, 59),
            # 'FinalizationDate': datetime.date(2019, 7, 11),
            # 'StartingDate': datetime.date(2019, 7, 11),
            # 'upTime': datetime.datetime(2019, 7, 21, 18, 41, 35),
            # 'rqTotal': 0,

            # # Junk?
            'Description': None,  # [None]
            'ProID': None,  # [1003909L, 1008929L, 1002911L, 1000405L, None]
            'RequestPDG': None,  # ['', None]
            'MasterID': None,  # None
            'ParentID': None,  # None
        }
        if self.id is not None:
            request['RequestID'] = self.id
        request['Inform'] = ','.join(self.inform_emails)
        if self.mc_config_version is None:
            request['Extra'] = None  # Pickled
        else:
            request['Extra'] = dump_fcn({'mcConfigVersion': self.mc_config_version})

        if self.type == "AnalysisProduction":
            request["SimCondDetail"] = self.input_dataset.conditions_dict
            request["SimCondID"] = self.input_dataset.conditions_id
            request["SimCondition"] = self.input_dataset.conditions_description
            request["EventType"] = self.input_dataset.event_type
            request["NumberOfEvents"] = -1
            request["RealNumberOfEvents"] = request["NumberOfEvents"]
            request['HasSubrequest'] = 0
        elif self.type == "Simulation":
            request.update(_lookup_simulation_condition({
                "SimDescription": self.sim_description
            }))
            if len(self.event_types) == 1:
                request["EventType"] = self.event_types[0]
                request["NumberOfEvents"] = self.n_events[0]
                request["RealNumberOfEvents"] = request["NumberOfEvents"]
                request['HasSubrequest'] = 0
            elif len(self.event_types) != len(self.n_events) and len(self.n_events) != 1:
                raise TypeError(
                    "event_types and n_events must have the same length",
                    self.event_types,
                    self.n_events,
                )
            elif len(self.n_events) == 0:
                raise TypeError(self.n_events)
            else:
                request["EventType"] = None
                request["NumberOfEvents"] = None
                request["RealNumberOfEvents"] = 0
                request['HasSubrequest'] = 1
                if len(self.n_events) == 1:
                    self.n_events = self.n_events * len(self.event_types)
                if self.sub_productions:
                    raise NotImplementedError()
                sub_productions = []
                for event_type, n_events in zip(self.event_types, self.n_events):
                    sub_prod = SubProduction(self)
                    sub_prod.event_type = event_type
                    sub_prod.n_events = n_events
                    sub_productions.append(sub_prod)
                self.sub_productions = sub_productions
        else:
            raise NotImplementedError(self.type)
        request["SimCondDetail"] = dump_fcn(request["SimCondDetail"])

        request['ProDetail'] = {'pAll': [], 'pDsc': []}
        for i, step in enumerate(self.steps, start=1):
            detail, p_all, p_dsc = step._to_production_manager_dict(i)
            request['ProDetail'].update(detail)
            request['ProDetail']['pAll'].append(p_all)
            if p_dsc is not None:
                request['ProDetail']['pDsc'].append(p_dsc)

        request['ProDetail']['pAll'] = ','.join(request['ProDetail']['pAll'])
        if self.processing_pass is None:
            request['ProDetail']['pDsc'] = '/'.join(request['ProDetail']['pDsc'])
        else:
            request['ProDetail']['pDsc'] = self.processing_pass
        request['ProPath'] = request['ProDetail']['pDsc']
        assert len(request['ProPath']) < 100
        request['ProDetail'] = dump_fcn(request['ProDetail'])

        return request
