#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
from pprint import pprint

from dirac_prod.classes import Production
from dirac_prod.schema import load


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    parser.add_argument("--submit", action="store_true")
    parser.add_argument("--register-steps", action="store_true")
    args = parser.parse_args()

    productions = load(args.filename)
    for data in productions:
        production = Production.from_yaml(**data)

        if args.submit:
            production.submit(register_steps=args.register_steps)
            print("Submitted production with ID", production.id)
            if production.sub_productions:
                print(
                    "   Sub-productions:",
                    [sp.id for sp in production.sub_productions],
                )
        elif args.register_steps:
            production.check_steps(register_steps=True)
        else:
            print("*" * 80)
            pprint(production._to_dict())


if __name__ == "__main__":
    parse_args()
