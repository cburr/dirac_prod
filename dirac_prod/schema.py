from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from strictyaml import Bool, Enum, Int, Float, Map
from strictyaml import Optional as Opt
from strictyaml import Regex, Seq, Str
from strictyaml import UniqueSeq as Set
from strictyaml import dirty_load

RE_APPLICATION = r"([A-Za-z]+)/(v\d+r\d+(?:p\d+)?(?:g\d+)?)"

wgs = ["Charm", "SL", "RTA", "QEE", "RD"]
mc_versions = ["2009", "2010", "2011", "2012", "2013", "2015", "2016", "2017", "2018", "Dev", "Upgrade"]
known_options_formats = ["l0app", "Tesla", "merge", "wgprod"]


def load(filename):
    schemas = _make_schemas()

    with open(filename) as fp:
        data = fp.read()
    productions = dirty_load(data, label=filename, allow_flow_style=True)
    for prod in productions:
        prod.revalidate(schemas[prod["type"].data])
    return productions.data


def _make_schemas():
    schemas = {
        "Simulation": _make_schema_simulation,
        "AnalysisProduction": _make_schema_ap,
    }

    step_schema = {
        "name": Str(),
        "application": Regex(RE_APPLICATION),
        "options": Seq(Str()),
        "data_pkgs": Seq(Str()),
        "input_types": Seq(Str()),
        "output_types": Seq(Str()),
        "processing_path": Regex(".{5,100}"),
        Opt("visible", default=False): Bool(),
        Opt("opt_fmt"): Enum(known_options_formats),
    }

    base_schema = {
        "type": Enum(list(schemas)),
        "name": Str(),
        "inform": Set(Str()),
        Opt("dbtags"): Map({"DDDB": Str(), "CondDB": Str()}),
        "steps": Seq(Map(step_schema)),
    }

    return {k: v(base_schema) for k, v in schemas.items()}


def _make_schema_simulation(base_schema):
    schema = base_schema.copy()
    schema.update({
        "wg": Enum(wgs),
        "mc_version": Enum(mc_versions),
        Opt("fast_simulation_type"): Enum(["ReDecay"]),
        "priority": Enum(["1a", "1b", "2a", "2b"]),
        "sim_condition": Regex(".{5,100}"),
        "event_types": Set(Regex(r"^\d{8}")),
        "num_events": Int(),
        Opt("retention_rate", default=1): Float(),
    })
    return Map(schema)


def _make_schema_ap(base_schema):
    input_dataset_schema = {
        "bk_query": Str(),
    }
    schema = base_schema.copy()
    schema.update({
        "wg": Enum(wgs),
        "priority": Enum(["1a", "1b", "2a", "2b"]),
        "input_dataset": Map(input_dataset_schema),
    })
    return Map(schema)
