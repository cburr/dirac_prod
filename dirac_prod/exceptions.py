from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


class NotRegisteredError(ValueError):
    pass


class AlreadyRegisteredError(ValueError):
    pass


class DiracError(Exception):
    pass
